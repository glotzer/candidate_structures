import os
import chimera
import argparse

def main(args):
    fn = args.fn

    chimera.runCommand('open ' + fn)

    # To generate a segment map
    from Segger import segment_dialog
    d = segment_dialog.show_volume_segmentation_dialog()
    d.numSteps.set(4)
    d.stepSize.set(1)
    d.minRegionSize.set(1)
    d.dmap.set(fn.split('.')[0])
    d.Segment()

    # Save the segmentation map
    smod = d.CurrentSegmentation()
    smod.path = smod.name
    d.SaveSegmentation()

    # Quit
    chimera.runCommand("stop")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("fn",
            help="The file to parse",
            type=str)
    main(parser.parse_args())
