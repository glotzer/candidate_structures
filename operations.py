import project as flow_project

def ionize(job):
    """Exploit the pexpect module to run.
    Somewhat messy currently to use the project level data, but I don't have a better version for now"""
    import pexpect
    with job:
        cmd='gmx genion -s {io_config} -o {ionized_gro} -p -pname {pname} -nname {nname} -neutral -conc {conc}'.format(
                io_config=flow_project.ionization_config, ionized_gro=flow_project.ionized_file,
                pname=flow_project.pname, nname=flow_project.nname, conc=job.sp.salt_concentration)
        child = pexpect.spawn(cmd)
        child.expect('Select a group:*')
        child.send('13\n')
        print(child.before.decode('ascii'))
        print(child.after.decode('ascii'))
        child.expect(pexpect.EOF)
        print(child.before.decode('ascii'))

def add_restraints(job):
    import tempfile
    import subprocess
    import glob

    restraint_index = 0
    with job:
        for fn in glob.glob("*.itp"):
            # Don't modify the restraint file
            if "posre" in fn:
                continue
        # USED TO REQUEST INPUT, NOT ANYMORE
        #    else:
        #        cont = input("Do you wish to modify this file: {}? Type yes or no.\n".format(fn))
        #        while cont not in ['yes', 'no']:
        #            cont = input("Please enter either yes or no.\n".format(fn))
        #        if cont == 'no':
        #            continue

            with open(fn, 'r') as f:
                contents = f.readlines()

            i = 0
            found_first = False
            found_second = False

            disre_string = "[ distance_restraints ]\n; ai aj type index type’ low up1 up2 fac\n{} {} 1    {}     1     10.0 31.0 50.0 1.0"

            while i < len(contents):
                line = contents[i]

                # first logic is for finding the atoms to constrain
                if 'residue  64' in line:
                    while True:
                        i += 1
                        line = contents[i]

                        # Break if done, continue if we already found the oxygen
                        if 'residue  68' in line:
                            break
                        elif found_first:
                            continue

                        # See if we have the oxygen
                        sp = line.split()
                        if sp[4] == 'O':
                            found_first = True
                            ai = sp[0]

                    # Sanity checks
                    if not found_first:
                        raise RuntimeError("Did not find the oxygen in residue 64")
                    elif 'residue  68' not in line:
                        raise RuntimeError("Not on the residue 68 line!")

                    # Now search for second one
                    while True:
                        i += 1
                        line = contents[i]

                        # Break if done, continue if we already found the oxygen
                        if 'residue  69' in line:
                            break
                        elif found_second:
                            continue

                        # See if we have the oxygen
                        sp = line.split()
                        if sp[4] == 'O':
                            found_second = True
                            aj = sp[0]

                    # Sanity checks
                    if not found_second:
                        raise RuntimeError("Did not find the oxygen in residue 68")
                    elif 'residue  69' not in line:
                        raise RuntimeError("Not on the residue 69 line!")

                i+=1

            # Now loop over the testfile
            #tf = tempfile.NamedTemporaryFile(mode="wt")
            #with tf as fw:
            tf = 'testfile.txt'
            with open(tf, 'wt') as fw:
                for line in contents:
                    if 'bonds' not in line:
                        fw.write(line)
                    else:
                        # Write the distance restraints before writing the line
                        fw.write(disre_string.format(ai, aj, restraint_index))
                        fw.write("\n\n")
                        fw.write(line)

            subprocess.call(['mv', tf, fn])
            restraint_index += 1
        job.document['restrained'] = True

def auto(job):
    "This is a meta-operation to execute multiple operations."
    project = get_project()
    logger.info("Running meta operation 'auto' for job '{}'.".format(job))
    for i in range(10):
        next_op = project.next_operation(job)
        if next_op is None:
            logger.info("No next operation, exiting.")
            break
        else:
            logger.info("Running next operation '{}'...".format(next_op))
            func = globals()[next_op.name]
            func(job)
    else:
        logger.warning("auto: Reached max # operations limit!")

if __name__ == '__main__':
    import flow
    flow.run()
