# About
This project contains code and data relating to the determination of the structure of the protomer found by the Ellington group.
In particular, this project involves the creation of various candidate structures for the protomer by fitting to the electron density data we have, and then evaluating the stability of each of these candidates via atomistic simulation.

After the initial simulations indicated that the atomistic simulations were unstable, this project was repurposed to provide the candidate structures for coarse-grained rigid body simulations.
These rigid body simulations use the PDB files for the protomers as well as the PDB files for individual proteins.

# Files included
The ConfigFiles folder contains the main files required to get started with generating a new configuration, including the positively and negatively charged pdb files and the density map.
Additional notes:
* The positive GFP is the 2B3P\_A\_min\_RK\_-1.34\_-1.01\_conserv.pdb file, while the negative GFP is the 2B3P\_A\_min\_DE\_-0.52\_-0.66\_conserv.pdb.
* The positive protein that I use in production runs is the Cerulean mutant, Ceru.pdb
* There are various mutants, which are named according to the table from the ARO proposal
  * IMPORTANT: The mutants are named according to the original mutation, i.e. the change from WT to the supercharged Ceru. However, the actual mutations I have performed are reversions back to the WT to see which mutations in the superfolder are responsible for its stability.
* The gen\_segmentation.py script can be used to generation the segment map from the mrc file, and the results of one such run are stored in the seg file. This segmentation is the one used to generate all of the sample protomers.



Scripts are stored in the scripts/ directory, which contains the following:
* The fit\_segments.py script generates protomer configuration by using Chimera's functionality to fit proteins to the density map.
* The init.py script calls the fit\_segments.py script for a range of possible configurations to generate the samples that we want.
  Each sample configuration is then placed in the appropriate folder in the signac-organized workspace.
* The add\offsets script was added after the fact to add the upper\_offset and lower\_offset variables to the statepoints instead of just the list of positive and negative segments.

The project (project.py) defines numerous operations, most of which just call GROMACS.
The operations.py script contains the logic for a few operations that either work independently of GROMACS, like the distance restraints script, or that add logic on top of GROMACS, like the addition of ions that uses pexpect to dynamically interact with the GROMACS command.

** For generating candidate structures with topology and nothing else, the only operation needed is the first; simply run python operations.py pdb2gmx to generate topologies for all structures in the workspace **

## Workspaces

* bkp\_workspace: A backup of the initially constructed workspace, just contains the different protomers.
* minimized\_workspace: A backup of the workspace after the minimization step, saved in case anything goes wrong with protomer generation.

# Requirements
* signac 0.9.1
* signac-flow 0.5.5
* Chimera: chimera production version 1.12 (build 41623) 2017-10-24 23:35:37 UTC
* GROMACS: version 5.1.4

[//]: # This is the rsync command to exclude all nonessential files when transferring to a remote computer
[//]: # rsync -av candidate_stability candidate_stability_rsync --exclude="*.trr" --exclude="*.tpr" --exclude="*.log" --exclude="*.xtc" --exclude="*.edr" --exclude="*.gro" --exclude="*.mdp" --exclude="*.cpt" --exclude "step*.pdb" --exclude="*.xvg"  --exclude="*#*#"
