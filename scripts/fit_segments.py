"""Script to fit proteins to the density cloud based on the chosen logic"""
import os
import argparse
import json
try:
    import chimera
except ImportError:
    print('Running CLI, not in Chimera. This script must be run using the `chimera` executable contained in the Chimera application')
    raise

def main(folder, rotation, upper_offset, lower_offset, pos_file, output_file='proposed_protomer.pdb'):
    """Generate protomer

    Args:
        folder (string): The folder containing all the requisite files
        rotation (int): The number of rotations to allow the fitting algorithm in Chimera to use
        upper_offset (bool): Whether to shift the indices where negative proteins are inserted
                             in the upper ring ("not upper_offset" therefore controls positive proteins)
        lower_offset (bool): Whether to shift the indices where negative proteins are inserted
                             in the lower ring ("not lower_offset" therefore controls positive proteins)
        pos_file (str): The positive protein file to use
        output_file (str): The output file
    """
    # Hardcoded based on segmentation map in use, MAKE SURE TO CHECK
    upper_ring = [106, 102, 93, 109, 87, 108, 110, 103]
    lower_ring = [86, 104, 101, 107, 100, 99, 83, 105]

    positive_GFP = os.path.join(folder, pos_file)
    negative_GFP = os.path.join(folder, '2B3P_A_min_DE_-0.52_-0.66_conserv.pdb')
    mrc_file = folder + "/run1_it025_class003_scale.mrc"
    seg_file = folder + "/run1_it025_class003_scale.seg"

    # For the statepoint
    positive_segments = []
    negative_segments = []

    # Open density map
    chimera.runCommand('open ' + mrc_file)

    # Open the segment map
    from Segger import segment_dialog
    d = segment_dialog.show_volume_segmentation_dialog()
    d.OpenSegFiles([(seg_file, 'Segmentation')])
    d.MapSelected(chimera.openModels.list()[0]) # Just assuming that the mrc will be at position 0

    # Fit the proteins to the map
    # First opening PDBs
    num_copies = 8
    if num_copies//2 != num_copies/2:
        raise RuntimeError("The number of copies per iteration must be divisible by two")

    from Segger import fit_dialog
    f = fit_dialog.show_fit_segments_dialog()

    # For now, all proteins will be fit using the same method each time 
    if rotation:
        f.rotaSearch.set(1)
        f.rotaSearchNum.set(rotation)
    else:
        f.rotaSearch.set(0)

    # Negative proteins in upper ring
    for i in range(num_copies//2):
        # The negative proteins
        chimera.runCommand('open ' + negative_GFP)
        mlist = chimera.openModels.list(modelTypes = [chimera.Molecule])
        f.struc.set(f.menu_name(mlist[-1]))
        idx = 2*i + upper_offset
        chimera.runCommand("select #1: " + str(
            upper_ring[idx])
            )
        f.Fit()
        negative_segments.append(upper_ring[idx])

        chimera.runCommand('open ' + negative_GFP)
        mlist = chimera.openModels.list(modelTypes = [chimera.Molecule])
        f.struc.set(f.menu_name(mlist[-1]))
        idx = 2*i + lower_offset
        chimera.runCommand("select #1: " + str(
            lower_ring[idx])
            )
        f.Fit()
        negative_segments.append(lower_ring[idx])

        # The positive proteins
        chimera.runCommand('open ' + positive_GFP)
        mlist = chimera.openModels.list(modelTypes = [chimera.Molecule])
        f.struc.set(f.menu_name(mlist[-1]))
        idx = 2*i + (not upper_offset)
        chimera.runCommand("select #1: " + str(
            upper_ring[idx])
            )
        f.Fit()
        positive_segments.append(upper_ring[idx])

        chimera.runCommand('open ' + positive_GFP)
        mlist = chimera.openModels.list(modelTypes = [chimera.Molecule])
        f.struc.set(f.menu_name(mlist[-1]))
        idx = 2*i + (not lower_offset)
        chimera.runCommand("select #1: " + str(
            lower_ring[idx])
            )
        f.Fit()
        positive_segments.append(lower_ring[idx])

    # Combine the models
    model_numbers = ' '.join("#" + str(mlist[i].id) for i in range(len(mlist)))
    cmd = "combine '" + model_numbers + "' name " + output_file
    chimera.runCommand(cmd)

    # Save output. Abusing the fact that the open items should be the model and the proteins and nothing else
    mlist = chimera.openModels.list(modelTypes = [chimera.Molecule])
    chimera.runCommand("write #" + str(mlist[-1].id) + " " + output_file)

    # Print out the statepoint and quit
    print(json.dumps(
        dict(
            positive_segments=positive_segments,
            negative_segments=negative_segments
            )
        ))

    chimera.runCommand("stop")
    print("Upper offset = {}, lower_offset = {}".format(upper_offset, lower_offset))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("folder",
            help="The folder containing the core data, including the pdbs and the segmentation map",
            type=str)
    parser.add_argument('rotation',
            help="How many rotational fits to try. The default (0) aligns principal axes",
            type=int)
    parser.add_argument('--upper-offset',
            help="Whether to offset the upper ring by one",
            action='store_true')
    parser.add_argument('--lower-offset',
            help="Whether to offset the lower ring by one",
            action='store_true')
    parser.add_argument('--pos-file',
            help="The name of the file containing the positive protein to use",
            type=str)
    parser.add_argument('--output-file',
            help="The file to output to",
            default='proposed_protomer.pdb',
            type=str)
    parser.set_defaults(upper_offset=False, lower_offset=False)
    args = parser.parse_args()
    main(args.folder, args.rotation, args.upper_offset, args.lower_offset, args.pos_file, output_file=args.output_file)
