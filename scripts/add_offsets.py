#!/usr/bin/python
"""This helper script was used to add the offsets to the statepoint
once that was deemed useful"""
import signac
project = signac.get_project()

convert = dict([
        ((106, 104, 93, 107, 87, 99, 110, 105), (True, False)), # No
        ((102, 104, 109, 107, 108, 99, 103, 105), (False, False)), # Yes
        ((102, 86, 109, 101, 108, 100, 103, 83), (False, True)), # No
        ((106, 86, 93, 101, 87, 100, 110, 83), (True, True)) # Yes
        ])

for job in project:
    mapping = convert[tuple(job.statepoint().get('positive_segments'))]
    job.sp.upper_offset = mapping[0]
    job.sp.lower_offset = mapping[1]
