#!/usr/bin/env python
"""Initialize the project's data space."""
from __future__ import print_function, division

import argparse
import signac
import shutil
import subprocess
import os
import json

def main_gfp(args):
    """This was the original main used when I was running with GFP for the positive protein"""
    try:
        project = signac.get_project()
    except LookupError:
        project = signac.init_project('CandidateStability')

    statepoints_init = []

    config_folder = project.fn("ConfigFiles")
    output_file = 'proposed_protomer.pdb'
    rotations = range(0, 121, 20)
    concentrations = [20, 50, 100, 150, 200]

    pos_type = "GFP"
    pos_file = '2B3P_A_min_RK_-1.34_-1.01_conserv.pdb'
    for upper_offset in [True, False]:
        for lower_offset in [True, False]:
            for rot in rotations:
                cmd = ["/Applications/Chimera.app/Contents/MacOS/chimera"]
                cmd.append("--nogui")
                cmd.append("--script")
                inner_cmd = [] # The command for the python script Chimera will run
                inner_cmd.append(project.fn("scripts/fit_segments.py"))
                inner_cmd.append(config_folder )
                inner_cmd.append(str(rot))
                if upper_offset:
                    inner_cmd.append("--upper-offset")
                if lower_offset:
                    inner_cmd.append("--lower-offset")
                inner_cmd.append("--pos-file " + pos_file)
                inner_cmd.append("--output-file " + output_file)
                cmd.append('"' + ' '.join(inner_cmd) + '"')
                print("Running command: " + ' '.join(cmd))

                # Extract statepoint from last line of output
                output = subprocess.check_output(' '.join(cmd), shell = True)
                lines = output.split(b"\n")
                sp_base = json.loads(lines[-2])

                # Augment statepoint and print the current status
                sp_base['rotation'] = rot
                sp_base['pos_type'] = pos_type
                sp_base['upper_offset'] = upper_offset
                sp_base['lower_offset'] = lower_offset
                print("The statepoint corresponding to the just completed run for upper_offset = {}, lower_offset = {}, rot = {} is the following: {}".format(upper_offset, lower_offset, rot, sp_base))
                created_pdb = os.getcwd() + "/" + output_file

                # Loop over the jobs and create jobs for every desired concentration
                for conc in concentrations:
                    sp = sp_base.copy()
                    sp['salt_concentration'] = conc/1000
                    job = project.open_job(sp)
                    job.init()
                    print(created_pdb)
                    print(job.fn(output_file))
                    shutil.copyfile(created_pdb, job.fn(output_file))
                    statepoints_init.append(sp)
                os.remove(created_pdb)

    # Writing statepoints to hash table as backup
    project.write_statepoints(statepoints_init)

def main_ceru(args):
    """Initialize the data space with the Cerulean protein"""
    try:
        project = signac.get_project()
    except LookupError:
        project = signac.init_project('CandidateStability')

    statepoints_init = []

    config_folder = project.fn("ConfigFiles")
    output_file = 'proposed_protomer.pdb'

    pos_type = "Ceru"
    all_mutations = {  # The list of mutations we want to reverse
            "negative": [
                ["T38D", "T43D", "V193D"],
                ["N149E", "N164E"],
                ["R80E", "K156D", "V193D"],
                ["A228D", "H231E"]
                ],
            "positive": [
                ["E7R", "T10R", "V12K"],
                ["V12K", "D118R"],
                ["D20K", "I129R"],
                ["D134K", "E143R"],
                ["I129R", "D134K", "D198R"], # This is the special one that our collabs are making
                ["Q158R", "N164K", "D198R"]
                ]
            }
    # Add in all individual mutations
    for chain, mutation_list in all_mutations.items():
        if chain == "positive"  and "I129R" in mutation_list:
            # Don't bother to split the special mutant since they're only prepping the one
            continue
        single_mutations = [[mutation] for mutation_set in mutation_list for mutation in mutation_set]
        all_mutations[chain].extend(single_mutations)
        # Avoid duplicates of individual mutations that are present in multiple groups
        all_mutations[chain] = list(list(x) for x in set(tuple(x) for x in all_mutations[chain]))

    pos_type = "Ceru"
    configs = [(False, False), (True, True), (False, True), (True, False)]
    for upper_offset, lower_offset in configs:
        # The offset combinations determined to work from previous runs with GFP (not Ceru)

        # First run with no mutation
        pos_file = "Ceru.pdb"
        rot = 0 # Hardcode since we don't care about rotation
        cmd = ["/Applications/Chimera.app/Contents/MacOS/chimera"]
        cmd.append("--nogui")
        cmd.append("--script")
        inner_cmd = [] # The command for the python script Chimera will run
        inner_cmd.append(project.fn("scripts/fit_segments.py"))
        inner_cmd.append(config_folder )
        inner_cmd.append(str(rot))
        if upper_offset:
            inner_cmd.append("--upper-offset")
        if lower_offset:
            inner_cmd.append("--lower-offset")
        inner_cmd.append("--pos-file " + pos_file)
        inner_cmd.append("--output-file " + output_file)
        cmd.append('"' + ' '.join(inner_cmd) + '"')
        print("Running command: " + ' '.join(cmd))

        # Extract statepoint from last line of output
        output = subprocess.check_output(' '.join(cmd), shell = True)
        lines = output.split(b"\n")
        sp = json.loads(lines[-2])

        # Augment statepoint and print the current status
        sp['rotation'] = rot
        sp['pos_type'] = pos_type
        sp['upper_offset'] = upper_offset
        sp['lower_offset'] = lower_offset
        print("The statepoint corresponding to the just completed run for upper_offset = {}, lower_offset = {}, rot = {} is the following: {}".format(upper_offset, lower_offset, rot, sp))
        created_pdb = os.getcwd() + "/" + output_file

        # Loop over the mutations
        job = project.open_job(sp)
        if not job in project:
            # Don't recreate if unnecessary
            job.init()
            print("Moving created file from {} to {}".format(created_pdb, job.fn(output_file)))
            shutil.copyfile(created_pdb, job.fn(output_file))
            statepoints_init.append(sp)
        os.remove(created_pdb)

        # Skip all mutations for now
        continue
        # Now loop over all mutations
        for chain, mutation_list in all_mutations.items():
            for mutation_set in mutation_list:
                # Hardcoding for now, can generalize later
                if not (chain == "positive" and ("D134K" in mutation_set or "E143R" in mutation_set)):
                    continue
                print("Running for: ", chain, mutation_set)
                pos_file = "Ceru_" + '_'.join(mutation_set) + '.pdb'
                print("pos_file: ", pos_file)

                cmd = ["/Applications/Chimera.app/Contents/MacOS/chimera"]
                cmd.append("--nogui")
                cmd.append("--script")
                inner_cmd = [] # The command for the python script Chimera will run
                inner_cmd.append(project.fn("scripts/fit_segments.py"))
                inner_cmd.append(config_folder )
                inner_cmd.append(str(rot))
                if upper_offset:
                    inner_cmd.append("--upper-offset")
                if lower_offset:
                    inner_cmd.append("--lower-offset")
                inner_cmd.append("--pos-file " + pos_file)
                inner_cmd.append("--output-file " + output_file)
                cmd.append('"' + ' '.join(inner_cmd) + '"')
                print("Running command: " + ' '.join(cmd))

                # Extract statepoint from last line of output
                output = subprocess.check_output(' '.join(cmd), shell = True)
                lines = output.split(b"\n")
                sp = json.loads(lines[-2])

                # Augment statepoint and print the current status
                sp['mutated_chain'] = chain
                sp['mutation'] = mutation_set
                sp['rotation'] = rot
                sp['pos_type'] = pos_type
                sp['upper_offset'] = upper_offset
                sp['lower_offset'] = lower_offset
                print("The statepoint corresponding to the just completed run for upper_offset = {}, lower_offset = {}, rot = {} is the following: {}".format(upper_offset, lower_offset, rot, sp))
                created_pdb = os.getcwd() + "/" + output_file

                # Loop over the mutations
                job = project.open_job(sp)
                if not job in project:
                    inp = input("Going to move, type y/n")
                    if inp == 'y':
                        job.init()
                        print("Moving created file from {} to {}".format(created_pdb, job.fn(output_file)))
                        shutil.copyfile(created_pdb, job.fn(output_file))
                        statepoints_init.append(sp)
                os.remove(created_pdb)

    # Writing statepoints to hash table as backup
    project.write_statepoints(statepoints_init)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Initialize the data space.")
    args = parser.parse_args()

    #main_gfp(args)
    main_ceru(args)
